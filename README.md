# Téléchargement du manga "One Piece" en scan

Ce script télécharge à partir du site [https://onepiecescan.fr/](https://onepiecescan.fr/) l'intégral du manga "One Piece".

Le numéro du dernier chapitre est paramétrable dans le script via une variable `MAX`

Si le script est arrêté, il reprendra là où il s'est arrêté dans le dernier chapitre où il manque des pages.
