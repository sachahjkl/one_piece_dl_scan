#!/bin/bash

MAX=1050
DIR="one_piece_scans"

mkdir -p $DIR
pushd "$DIR" || exit
for chapitre in $(seq $MAX)
do
	echo "Téléchargement du chapitre $chapitre :"

	mkdir -p "$chapitre"
	pushd "$chapitre" || exit

	page=1
	while true
	do
		page_file="$(printf "%02d" $page).jpg"
		url="https://one-piece-manga.fr/comic/$chapitre/$page_file"

		printf "\ttéléchargement page %s via URL \"%s\"\n" "$page" "$url"
		page=$((page+1))

		[ -f "$page_file" ] && continue 
		curl -O --fail "$url"
		res=$?
		printf "\tcode d'erreur de téléchargement de la page : %s\n" $res
		test "$res" != "0" && break

	done
	popd || exit
done
popd || exit
